# CtrlStream

This program stores MIDI/OSC streams in a textfile. The item interval between datapoints can be set in ms as needed.

[→ go to File Structure](#markdown-header-file-structure)

## Credits

This is currently used as part of the *ConductingSpaces* project, which is itself part of the larger project [*Atlas of Smooth Spaces*](https://www.the-smooth.space/) at the University of Music and Performing Arts Vienna.

![Repo:ATLAS:Ctrl:Stream](https://docs.google.com/drawings/d/e/2PACX-1vQ-cHocUObMvFQClW0ANR5ZBf8YEYNrBVNQ8HUnkXApQpTqNJqTBOdchJGM-_MbmX3weIaGPLDxKcKX/pub?w=328&h=303)

# Instalation and usage

### (MacOS)

This repository contains a MaxMSP project that can be extended ot customised as needed. A standalone (CtrlStream.app) for MacOS is also included.

### Windows

If you are a Windows user, you may clone the repository and open the MaxMSP project 
with your Windows version of the software. A windows standalone version will be included in the future.

## Usage

You may set the path to the folder where you would like the streamed data to be stored using the **select saving folder** button. By default, this path is the same as where the patches (or standalone app) are stored.

- Set the OSC receiving port
- Start recording (**Start/Stop**)
- Stop (**Start/Stop**)

## Customisation

# File structure

Data is stored as a simple **.txt** file (without separating characters). Each line represents a snapshot at regular time intervals. The interval itself can be set using the GUI. The header line of the file has a descriptive string for each column. All columns are populated. Here is an example file included in the repository:

[CtrlStream_2021-09-09_16h46m10s.txt](CtrlStream/media/CtrlStream_2021-09-09_16h46m10s.txt)

### Stream vs. on/off data

For streams of data (such as XYZ coordinates), the stored data will be the raw float provided by the sending device. For abstract states (such as *Fist posture* in Glover) which can be de/activated only, we use boolean 1/0 values. For states that are self-excluding, such as up-down, forwards-backwards and left-right, one value is expressive enough. In the case of *Glover's* directions, I have used values 1-6 to refer to the aforementioned states.



# MobMuPlat

Setting up the device:

- In "broadcast IP address" click "Reset to default multicast address"

- set Output port

- set Input port (optional)





# Contact

adrian@neuesatelier.org

____

# To-Do

- Adapt to the Effects format
