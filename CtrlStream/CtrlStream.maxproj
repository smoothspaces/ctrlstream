{
	"name" : "CtrlStream",
	"version" : 1,
	"creationdate" : 3714037381,
	"modificationdate" : 3728807098,
	"viewrect" : [ 97.0, 130.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"CtrlStream.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"DateTimeString.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"twoDigits.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"CurrentLocation.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"Sandbox.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"expandDevice.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"feat-selector.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Documents/Max 8/Projects/[Spaces]/SmoothOperator/CtrlStream/CtrlStream/patchers",
					"projectrelativepath" : "../../SmoothOperator/CtrlStream/CtrlStream/patchers"
				}

			}

		}
,
		"code" : 		{
			"media-path.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}
,
		"data" : 		{

		}
,
		"other" : 		{
			"2021-11-05_LeosHand2" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"2021-11-05_LeosHand" : 			{
				"kind" : "file",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1835887981,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
